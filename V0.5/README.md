# InterfaseDigital-pttlink

Este proyeto es una intefase para los puertos GPIO de la tarjeta de sonido a fin de utilizar PTTlink.

## Guía de uso

_To do..._

## Licencia

Este es un Diseño de Hardware Libre: puede redistribuirlo y/o modificarlo
bajo los términos de la Licencia Pública General GNU publicada por la
publicada por la Free Software Foundation, ya sea la versión 3 de la
(a su elección) cualquier versión posterior.

Este diseño de hardware libre se distribuye con la esperanza de que sea útil
pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
COMERCIALIZACIÓN o ADECUACIÓN A UN PROPÓSITO PARTICULAR. Consulte la
Licencia Pública General GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General de GNU
junto con este Diseño de Hardware.  Si no es así, consulte <https://www.gnu.org/licenses/>.
