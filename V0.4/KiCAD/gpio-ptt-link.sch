EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "GPIO para PTTLINK"
Date "2021-02-01"
Rev "0.4"
Comp "Radioaficionados en Español"
Comment1 ""
Comment2 "https://wodielite.com/wiki/index.php?title=Modificar_tarjeta_FOB"
Comment3 "Basado en el esquemático de XEIF"
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 600842B3
P 4250 2100
F 0 "R1" V 4043 2100 50  0000 C CNN
F 1 "3K3" V 4134 2100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4180 2100 50  0001 C CNN
F 3 "~" H 4250 2100 50  0001 C CNN
	1    4250 2100
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 6008481A
P 4000 2350
F 0 "R2" H 4070 2396 50  0000 L CNN
F 1 "330" H 4070 2305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3930 2350 50  0001 C CNN
F 3 "~" H 4000 2350 50  0001 C CNN
	1    4000 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60085653
P 5750 3700
F 0 "R3" V 5543 3700 50  0000 C CNN
F 1 "3K3" V 5634 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5680 3700 50  0001 C CNN
F 3 "~" H 5750 3700 50  0001 C CNN
	1    5750 3700
	0    1    1    0   
$EndComp
$Comp
L Transistor_BJT:PN2222A Q2
U 1 1 60087733
P 5300 3700
F 0 "Q2" H 5491 3746 50  0000 L CNN
F 1 "PN2222A" H 5491 3655 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 5500 3625 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 5300 3700 50  0001 L CNN
	1    5300 3700
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60087F7A
P 4000 2700
F 0 "D2" V 4039 2582 50  0000 R CNN
F 1 "LED" V 3948 2582 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 4000 2700 50  0001 C CNN
F 3 "~" H 4000 2700 50  0001 C CNN
	1    4000 2700
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:PN2222A Q1
U 1 1 60088983
P 4700 2100
F 0 "Q1" H 4890 2146 50  0000 L CNN
F 1 "PN2222A" H 4890 2055 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 4900 2025 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 4700 2100 50  0001 L CNN
	1    4700 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2100 4500 2100
Wire Wire Line
	4800 2400 4800 2300
Wire Wire Line
	4000 2200 4000 2100
Wire Wire Line
	4000 2100 4100 2100
Wire Wire Line
	4000 2550 4000 2500
$Comp
L power:GND #PWR04
U 1 1 600A074E
P 4000 2900
F 0 "#PWR04" H 4000 2650 50  0001 C CNN
F 1 "GND" H 4005 2727 50  0000 C CNN
F 2 "" H 4000 2900 50  0001 C CNN
F 3 "" H 4000 2900 50  0001 C CNN
	1    4000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2900 4000 2850
Connection ~ 4000 2100
Text GLabel 3450 2100 0    50   Input ~ 0
FOB_PTT
Wire Wire Line
	3450 2100 4000 2100
Wire Wire Line
	4800 1900 4800 1800
$Comp
L power:GND #PWR01
U 1 1 600ACEA5
P 4800 2400
F 0 "#PWR01" H 4800 2150 50  0001 C CNN
F 1 "GND" H 4805 2227 50  0000 C CNN
F 2 "" H 4800 2400 50  0001 C CNN
F 3 "" H 4800 2400 50  0001 C CNN
	1    4800 2400
	1    0    0    -1  
$EndComp
Text GLabel 6150 1800 2    50   Input ~ 0
PTT
Wire Wire Line
	5500 3700 5600 3700
Wire Wire Line
	5900 3700 5950 3700
Text GLabel 6100 3700 2    50   Input ~ 0
COR
$Comp
L power:GND #PWR07
U 1 1 600B552F
P 5200 4000
F 0 "#PWR07" H 5200 3750 50  0001 C CNN
F 1 "GND" H 5205 3827 50  0000 C CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "" H 5200 4000 50  0001 C CNN
	1    5200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4000 5200 3900
Text GLabel 3500 3500 0    50   Input ~ 0
FOB_COR
Text Notes 4800 1550 0    50   ~ 0
PTT Section
Text Notes 4800 3350 0    50   ~ 0
COR Section
Text GLabel 4800 4900 2    50   Input ~ 0
Audio_OUT_(TX)
Wire Wire Line
	4800 4900 3500 4900
Wire Wire Line
	3500 4700 4800 4700
Text Notes 4800 4500 0    50   ~ 0
Audio Section
Text GLabel 4800 4700 2    50   Input ~ 0
Audio_IN_(RX)
Wire Wire Line
	7250 1900 7500 1900
Text GLabel 7500 2200 2    50   Input ~ 0
Audio_OUT_(TX)
Text GLabel 7500 2100 2    50   Input ~ 0
Audio_IN_(RX)
Text GLabel 7500 1900 2    50   Input ~ 0
COR
Text GLabel 7500 2000 2    50   Input ~ 0
PTT
Text Notes 4800 5300 0    50   ~ 0
GND Section
Wire Wire Line
	3500 5650 3500 5500
$Comp
L power:GND #PWR010
U 1 1 600FDCFB
P 5100 5650
F 0 "#PWR010" H 5100 5400 50  0001 C CNN
F 1 "GND" H 5105 5477 50  0000 C CNN
F 2 "" H 5100 5650 50  0001 C CNN
F 3 "" H 5100 5650 50  0001 C CNN
	1    5100 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 5650 5100 5500
Wire Wire Line
	3500 5500 5100 5500
$Comp
L power:GND #PWR08
U 1 1 60109454
P 7500 2350
F 0 "#PWR08" H 7500 2100 50  0001 C CNN
F 1 "GND" V 7505 2222 50  0000 R CNN
F 2 "" H 7500 2350 50  0001 C CNN
F 3 "" H 7500 2350 50  0001 C CNN
	1    7500 2350
	-1   0    0    -1  
$EndComp
Text Notes 3200 1300 0    50   ~ 0
FOB
Text Notes 4850 1300 0    50   ~ 0
Interface
Wire Notes Line
	8450 1200 8450 5950
Wire Notes Line
	2350 1200 2350 5950
Text Notes 3100 1550 0    50   ~ 0
PTT Section
Text Notes 3150 3350 0    50   ~ 0
COR Section
Text Notes 3100 4500 0    50   ~ 0
Audio Section
Text Notes 3150 5300 0    50   ~ 0
GND Section
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60149E7D
P 6100 5500
F 0 "#FLG0101" H 6100 5575 50  0001 C CNN
F 1 "PWR_FLAG" H 6100 5673 50  0000 C CNN
F 2 "" H 6100 5500 50  0001 C CNN
F 3 "~" H 6100 5500 50  0001 C CNN
	1    6100 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6014A721
P 6100 5650
F 0 "#PWR0102" H 6100 5400 50  0001 C CNN
F 1 "GND" H 6105 5477 50  0000 C CNN
F 2 "" H 6100 5650 50  0001 C CNN
F 3 "" H 6100 5650 50  0001 C CNN
	1    6100 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5650 6100 5500
$Comp
L power:GND #PWR0101
U 1 1 6014D738
P 3500 5650
F 0 "#PWR0101" H 3500 5400 50  0001 C CNN
F 1 "GND" H 3505 5477 50  0000 C CNN
F 2 "" H 3500 5650 50  0001 C CNN
F 3 "" H 3500 5650 50  0001 C CNN
	1    3500 5650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Male J2
U 1 1 60161ACB
P 6900 5050
F 0 "J2" H 6700 4750 50  0000 C CNN
F 1 "FOB_Conector" H 6900 4650 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6900 5050 50  0001 C CNN
F 3 "~" H 6900 5050 50  0001 C CNN
	1    6900 5050
	1    0    0    1   
$EndComp
Text GLabel 7250 4850 2    50   Input ~ 0
FOB_PTT
Text GLabel 7250 5050 2    50   Input ~ 0
Audio_Out_(to_FOB_Input)
Text GLabel 7250 4950 2    50   Input ~ 0
FOB_COR
$Comp
L power:GND #PWR09
U 1 1 60167B3F
P 7200 5300
F 0 "#PWR09" H 7200 5050 50  0001 C CNN
F 1 "GND" H 7205 5127 50  0000 C CNN
F 2 "" H 7200 5300 50  0001 C CNN
F 3 "" H 7200 5300 50  0001 C CNN
	1    7200 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5300 7200 5250
Wire Wire Line
	7200 5250 7100 5250
Wire Notes Line
	6450 1200 6450 5950
Text Notes 7050 1350 0    50   ~ 0
Connector-Harness Section
Wire Wire Line
	7250 4850 7100 4850
Wire Wire Line
	7100 4950 7250 4950
Wire Wire Line
	7250 5050 7100 5050
Wire Wire Line
	7100 5150 7250 5150
Wire Notes Line
	3800 1200 3800 5950
Text GLabel 7250 5150 2    50   Input ~ 0
Audio_In_(from_FOB_Out)
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 600A95EB
P 7050 2100
F 0 "J1" H 6800 1800 50  0000 C CNN
F 1 "GPIO-Connector" H 7050 1700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 7050 2100 50  0001 C CNN
F 3 "~" H 7050 2100 50  0001 C CNN
	1    7050 2100
	1    0    0    1   
$EndComp
Wire Wire Line
	7500 2000 7250 2000
Wire Wire Line
	7250 2100 7500 2100
Wire Wire Line
	7500 2200 7250 2200
Wire Wire Line
	7250 2300 7500 2300
Wire Wire Line
	7500 2300 7500 2350
Text Notes 1000 6650 0    50   ~ 0
rev 0.1\n* Se cambia el conector J1 de DB25 a Header \n* Se actualiza el valor de R3 = 3k3\n* Se cambia el diodo D1 por un zener con encapsulado D035
Text Notes 1000 7100 0    50   ~ 0
rev 0.2\n* Se agrega diodo y capacitor en la seccion COR\n* Se agrega LED y resistencia en la seccion COR\n* Se invierten los conectores J1 y J2 para que el pin 1 sea GND\n* Se intercambia COR y PTT en J1 
Connection ~ 5350 1800
Wire Wire Line
	5850 1800 6150 1800
Connection ~ 5850 1800
Wire Wire Line
	5350 1800 5850 1800
Wire Wire Line
	4800 1800 5350 1800
$Comp
L Device:D_Zener D1
U 1 1 600A2172
P 5350 2100
F 0 "D1" V 5304 2180 50  0000 L CNN
F 1 "D_Zener" V 5395 2180 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 5350 2100 50  0001 C CNN
F 3 "~" H 5350 2100 50  0001 C CNN
	1    5350 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 2400 5850 2250
$Comp
L power:GND #PWR03
U 1 1 600AD5E5
P 5850 2400
F 0 "#PWR03" H 5850 2150 50  0001 C CNN
F 1 "GND" H 5855 2227 50  0000 C CNN
F 2 "" H 5850 2400 50  0001 C CNN
F 3 "" H 5850 2400 50  0001 C CNN
	1    5850 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 600AD435
P 5350 2400
F 0 "#PWR02" H 5350 2150 50  0001 C CNN
F 1 "GND" H 5355 2227 50  0000 C CNN
F 2 "" H 5350 2400 50  0001 C CNN
F 3 "" H 5350 2400 50  0001 C CNN
	1    5350 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1950 5350 1800
Wire Wire Line
	5850 1800 5850 1950
Wire Wire Line
	5350 2250 5350 2400
$Comp
L Device:C C1
U 1 1 60088E6C
P 5850 2100
F 0 "C1" H 5735 2054 50  0000 R CNN
F 1 "47pF" H 5735 2145 50  0000 R CNN
F 2 "Capacitor_THT:C_Disc_D6.0mm_W2.5mm_P5.00mm" H 5888 1950 50  0001 C CNN
F 3 "~" H 5850 2100 50  0001 C CNN
	1    5850 2100
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Zener D3
U 1 1 6016000B
P 4500 3800
F 0 "D3" V 4454 3880 50  0000 L CNN
F 1 "D_Zener" V 4545 3880 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 4500 3800 50  0001 C CNN
F 3 "~" H 4500 3800 50  0001 C CNN
	1    4500 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 4100 4500 3950
$Comp
L power:GND #PWR06
U 1 1 60160012
P 4500 4100
F 0 "#PWR06" H 4500 3850 50  0001 C CNN
F 1 "GND" H 4505 3927 50  0000 C CNN
F 2 "" H 4500 4100 50  0001 C CNN
F 3 "" H 4500 4100 50  0001 C CNN
	1    4500 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 60160018
P 4000 4100
F 0 "#PWR05" H 4000 3850 50  0001 C CNN
F 1 "GND" H 4005 3927 50  0000 C CNN
F 2 "" H 4000 4100 50  0001 C CNN
F 3 "" H 4000 4100 50  0001 C CNN
	1    4000 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3650 4000 3500
Wire Wire Line
	4500 3500 4500 3650
Wire Wire Line
	4000 3950 4000 4100
$Comp
L Device:C C2
U 1 1 60160021
P 4000 3800
F 0 "C2" H 3885 3754 50  0000 R CNN
F 1 "47pF" H 3885 3845 50  0000 R CNN
F 2 "Capacitor_THT:C_Disc_D6.0mm_W2.5mm_P5.00mm" H 4038 3650 50  0001 C CNN
F 3 "~" H 4000 3800 50  0001 C CNN
	1    4000 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 3500 4000 3500
Connection ~ 4000 3500
Wire Wire Line
	4000 3500 4500 3500
Connection ~ 4500 3500
Wire Wire Line
	4500 3500 5200 3500
$Comp
L Device:R R4
U 1 1 6017F444
P 5950 3950
F 0 "R4" H 6020 3996 50  0000 L CNN
F 1 "330" H 6020 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 3950 50  0001 C CNN
F 3 "~" H 5950 3950 50  0001 C CNN
	1    5950 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 6017F44A
P 5950 4300
F 0 "D4" V 5989 4182 50  0000 R CNN
F 1 "LED" V 5898 4182 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 5950 4300 50  0001 C CNN
F 3 "~" H 5950 4300 50  0001 C CNN
	1    5950 4300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5950 4150 5950 4100
$Comp
L power:GND #PWR011
U 1 1 6017F451
P 5950 4500
F 0 "#PWR011" H 5950 4250 50  0001 C CNN
F 1 "GND" H 5955 4327 50  0000 C CNN
F 2 "" H 5950 4500 50  0001 C CNN
F 3 "" H 5950 4500 50  0001 C CNN
	1    5950 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4500 5950 4450
Wire Wire Line
	5950 3800 5950 3700
Connection ~ 5950 3700
Wire Wire Line
	5950 3700 6100 3700
Text Notes 1000 7400 0    50   ~ 0
rev 0.3\n* Se cambaia la etiqueta global Audio_IN_(TX) por Audio_OUT_(TX)\n* Se cambaia la etiqueta global Audio_OUT_(RX) por Audio_IN_(RX)
Text Notes 1000 7700 0    50   ~ 0
rev 0.4\n* Se cambaia la etiqueta global FOB_Spkr_(OUT) por Audio In (from FOB Out)\n* Se cambaia la etiqueta global FOB_Mic_(IN) por Audio_Out_(to_ FOB_Input)
Text GLabel 3500 4900 0    50   Input ~ 0
Audio_In_(from_FOB_Out)
Text GLabel 3500 4700 0    50   Input ~ 0
Audio_Out_(to_FOB_Input)
Wire Notes Line
	2350 3200 6450 3200
Wire Notes Line
	2350 1200 8500 1200
Wire Notes Line
	2350 1400 8500 1400
Wire Notes Line
	2350 5150 6450 5150
Wire Notes Line
	2350 5950 8500 5950
$EndSCHEMATC
